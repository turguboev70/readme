# Haad Education

## Introduction

Welcome to the frontend of [Haad.uz](https://haad.uz/)! This site is built using modern web technologies such as React, Redux, and JavaScript. The goal of this project is to provide a seamless user experience with a clean, intuitive interface.

## Getting started

To get started, please make sure you have the following dependencies installed:

    - Node.js(version 12 or higher)
    - npm (version 6 or higher)

Once you have the dependencies installed, clone the repository and run the following commands:

``` sql
    npm install
    npm start
```

This will start a development server and open the site in your default web browser.

## Dependencies

This project uses the following dependencies:

    - React (version 17 or higher)
    - Redux (version 4 or higher)

## Folder structure

The project follows the following folder structure:

```css
    src/
    components/
        [Reusable components]
    pages/
        [Page-specific components]
    store/
        [Redux store and actions]
    utils/
        [Helper functions and utilities]
```

## Contact

If you have any questions or issues with the site, please contact the developer at [hikmatillo_dev@haad.uz].

